package com.ci.myShop.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ci.myShop.model.Item;

public class StorageTest {
	@Test
	public void addItem() {
		Storage storage = new Storage();
		Item objet1 = new Item("cahier", 80, 5, 50);
		storage.addItem(objet1);
		assertEquals(1,storage.getItemMap().values().size());
	}
	@Test
	public void getItem() {
		Storage storage = new Storage();
		Item objet1 = new Item("cahier", 80, 5, 50);
		storage.addItem(objet1);
		Item extractedItem=storage.getItem("cahier");
		assertEquals(extractedItem, objet1);
	}
	
}