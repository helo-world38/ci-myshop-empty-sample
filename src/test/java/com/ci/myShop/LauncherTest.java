package com.ci.myShop;

import static org.junit.Assert.assertEquals;

import com.ci.myShop.controller.Shop;
import com.ci.myShop.controller.Storage;
import com.ci.myShop.model.Item;

public class LauncherTest {
	
	public static void testShopSansStorage() {
		Shop magasin = new Shop(null, 0.0f) ;
		Item cahier = new Item ("Cahier", 3, 8, 3);
		assertEquals(false, magasin.buy(cahier));
	}
	
	public static void testShopSansCash() {
		Storage storage = new Storage();
		Shop magasin = new Shop(storage, 0.0f) ;
		Item cahier = new Item ("Cahier", 3, 8, 3);
		assertEquals(false, magasin.buy(cahier));
	}
	
	public static void main(String[] args) {
		
		//1er test : shop sans storage
		testShopSansStorage();
		//2eme test
		testShopSansCash();
		
			Storage storage = new Storage();
			Shop magasin = new Shop(storage, 0.0f) ;
			Item objet1 = new Item ("Stylo", 1, 3, 5);
			Item objet2 = new Item ("Carnet", 2, 5, 8);
		
			
			if (magasin.buy(objet2)) {
				System.out.println(magasin.display());
			}
			if (magasin.buy(objet2)) {
				System.out.println(magasin.display());
			}
			if (magasin.buy(objet1)) {
				System.out.println(magasin.display());
			}
		}
		
	}
