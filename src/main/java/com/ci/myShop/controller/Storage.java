package com.ci.myShop.controller;

import java.util.HashMap;
import java.util.Map;

import com.ci.myShop.model.Item;

public class Storage {

	Map<String, Item> itemMap;

	public Storage() {

		itemMap = new HashMap<String, Item>(); // string type de la cle Item = la valeur
	}

	public Map<String, Item> getItemMap() {
		return itemMap;
	}

	public void setItemMap(Map<String, Item> itemMap) {
		this.itemMap = itemMap;
	}

	// ajouter un item � l'itemMap
	public void addItem(Item obj) {
		itemMap.put(obj.getName(), obj);
	}

	// r�cuperer un objet item de l'itemMap
	public Item getItem(String name) {
		Item item = itemMap.get(name);
		return item;
	}

	// mettre en forme l'objet dans String
	public String display() {
		String contenuMap = "";
		for (Item item : itemMap.values()) { // parcours toutes les valeurs de la map
			contenuMap = contenuMap + item.display(); // enrichi le string contenuMap avec le display
		}
		contenuMap = contenuMap + "}";
		String result = "stock :" + contenuMap;
		return result;
	}

}
