package com.ci.myShop.controller;

import com.ci.myShop.model.Item;

public class Shop {
	private Storage storage;
	private float cash;
	
	public Shop (Storage storage, float cash) {
		super();
		this.storage = storage;
		this.cash = cash;
	}
	
	//vendre un item s'il existe
	public Item sell(String name) {
		//chercher item dans map + stocker la valeur name de la map
		Item item = storage.getItem(name);
		//verifier que l'item existe dans la map & que >0
		if (item != null && item.getNbrElt() > 0) {
			//alimenter cash avec le prix de l'item
			cash = cash + item.getPrice();
			//diminuer le stock
			item.setNbrElt(item.getNbrElt() - 1);
		}
		return item;
	}
	
	//fonction achat
	public boolean buy (Item item) {
		if (storage == null ) {
		System.out.println("storage is null can not buy");
		return false;
		}
		
		//verifier que le shop a suffisament de cash
		if (cash < item.getPrice()) {
			System.out.println("pas assez de cash");
			return false;
		}
		//diminuer le cash
		cash = cash - item.getPrice();

		// Recuperation item
		Item itemStorage = storage.getItem(item.getName());
	    
	    //si l'item pas deja en stock, on le cree
	    if(itemStorage == null){
	        storage.addItem(item);
	    } else {
	        //si non on augmente juste la quantite de l'item dans le stock
	    	itemStorage.setNbrElt(itemStorage.getNbrElt() + item.getNbrElt());
	    }
	    return true;
	}
		
		
	//mettre en forme l'objet dans une String
		public String display() {
			String result = "stock :" +storage.display()+ ", caisse :" +cash;
			return result;
		}

		public Storage getStorage() {
			return storage;
		}

		public void setStorage(Storage storage) {
			this.storage = storage;
		}

		public float getCash() {
			return cash;
		}

		public void setCash(float cash) {
			this.cash = cash;
		}
		
		
		
}
