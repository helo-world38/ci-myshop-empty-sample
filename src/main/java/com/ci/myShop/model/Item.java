package com.ci.myShop.model;

public class Item {
	private String name;
	private int id;
	private float price;
	private int nbrElt;
	
	
	public Item(String name, int id, float price, int nbrElt) {
		super();
		this.name = name;
		this.id = id;
		this.price = price;
		this.nbrElt = nbrElt;
	}
	
	//Ensemble des accesseurs
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getNbrElt() {
		return nbrElt;
	}

	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}
	
	//mettre en forme l'objet dans String
		public String display() {
			String result = "Nom :" +name+ ", id:" +id+ ", price:" +price+ ", nombre d'elements en stock:" +nbrElt;
			return result;
		}
}
