package com.ci.myShop;

import com.ci.myShop.controller.Shop;
import com.ci.myShop.model.Item;
import com.ci.myShop.controller.Storage;

public class Launch {

	public static void main(String[] args) {
		//creation de la liste de item 
		Item [] items =new Item [10]; 
		//exemple de item 
		Item item1 = new Item ("stylo" , 1,2,5);
		Item item2 = new Item ("cahier" , 1,5,5);
		//item2 dans la liste 
		items[0]=item2;
		//creation de shop
		Storage storage=new Storage();
		float cash =10;
		Shop shop =new Shop(storage, cash);
		shop.getStorage().addItem(item1);
		storage.addItem(item1);
		storage.getItem("cahier");
		shop.sell("cahier");	}}
 
